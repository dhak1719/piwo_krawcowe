import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const vuex = new Vuex.Store({
  state: {
    count: 5
  },
  mutations: {
    go: state => state.count++
  },
  actions: {}
});

export default vuex;

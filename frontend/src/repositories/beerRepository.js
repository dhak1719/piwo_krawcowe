import Repository from "./repository";

const resource = "/beers";

export default {
  get() {
    return Repository.get(`${resource}`);
  },
  getBeer(beerId) {
    return Repository.get(`${resource}/${beerId}`);
  },
  createBeer(beer) {
    return Repository.post(`${resource}`, beer);
  },
  updateBeer(beer) {
    return Repository.put(`${resource}/${beer._id}`, beer);
  },
  deleteBeer(beer) {
    return Repository.delete(`${resource}/${beer._id}`);
  }
};

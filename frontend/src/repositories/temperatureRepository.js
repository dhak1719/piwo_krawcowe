import Repository from "./repository";

const resource = "/temperature";

export default {
  getTemperatures(sensorId) {
    return Repository.get(`${resource}/${sensorId}`);
  }
};

import BeerRepository from "./beerRepository";
import TemperatureRepository from "./temperatureRepository";

const repositories = {
  beers: BeerRepository,
  temp: TemperatureRepository
};

export const RepositoryFactory = {
  get: name => repositories[name]
};

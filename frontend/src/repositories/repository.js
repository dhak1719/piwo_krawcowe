import axios from "axios";

const baseDomain = process.env.VUE_APP_API_URL;
const baseURL = `${baseDomain}`;

export default axios.create({
  baseURL

  // headers: { Authorization: `Bearer yourSuppaToken` }
  // https://codesandbox.io/s/o93j2y4mj9
});

import Vue from "vue";
import App from "./App";
import router from "./router";
import store from "./store";

import axios from "axios";
import VueAxios from "vue-axios";

import cognitoAuth from "./cognito-auth";
import repo from "./repositories/repository";

//console.log(repo);
//console.log(cognitoAuth);
console.log(store);
console.log(router);

Vue.config.productionTip = false;
Vue.use(VueAxios, axios);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

import Vue from "vue";
import Router from "vue-router";

import Home from "@/components/Home.vue";
import Contact from "@/components/Contact";
import Login from "@/components/Login";
import Register from "@/components/Register";
import NotFound from "@/components/NotFound";
import Beers from "@/components/beers/Beers";
import Beer from "@/components/beers/Beer";
import BeerAdd from "@/components/beers/BeerAdd";
import BeerEdit from "@/components/beers/BeerEdit";
import Monitor from "@/components/monitor/Monitor";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/beers",
      name: "Beers",
      component: Beers
    },
    {
      path: "/beers/:id",
      name: "Beer",
      component: Beer
    },
    {
      path: "/beer_add",
      name: "Beer",
      component: BeerAdd
    },
    {
      path: "/beer_edit/:id",
      name: "Beer",
      component: BeerEdit
    },
    {
      path: "/monitor",
      name: "Monitor",
      component: Monitor
    },
    {
      path: "/contact",
      name: "Contact",
      component: Contact
    },

    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/register",
      name: "Register",
      component: Register
    },
    {
      path: "/notFound",
      name: "NotFound",
      component: NotFound
    }
  ]
  // routes: [
  //   {
  //     path: "/",
  //     name: "home",
  //     component: Home
  //   },
  //   {
  //     path: "/about",
  //     name: "about",
  //     // route level code-splitting
  //     // this generates a separate chunk (about.[hash].js) for this route
  //     // which is lazy-loaded when the route is visited.
  //     component: () =>
  //       import(/* webpackChunkName: "about" */ "./views/About.vue")
  //   }
  // ]
});

router.beforeEach((to, from, next) => {
  const publicPages = ["/", "/login", "/notFound"];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = true;

  if (!to.matched.length) {
    return next("/notFound");
  }

  if (authRequired && !loggedIn) {
    return next("/login");
  }

  next();
});

export default router;

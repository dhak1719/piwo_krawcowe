'use strict';

module.exports = function (app) {
  var beers = require('../controllers/beerController');

  app.route('/beers')
    .get(beers.listAllBeer)
    .post(beers.createBeer);

  app.route('/beers/:beerId')
    .get(beers.readBeer)
    .put(beers.updateBeer)
    .delete(beers.deleteBeer);
};
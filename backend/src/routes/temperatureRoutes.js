'use strict';

module.exports = function (app) {
  var temperature = require('../controllers/temperatureController');

  app.route('/temperatures')
    .get(temperature.getTemperatures)
    .post(temperature.saveTemperatures);
}
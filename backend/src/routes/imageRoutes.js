'use strict';
var fs = require('fs');

module.exports = function (app, upload) {
  var image = require('../controllers/imageController');

  app.post('/upload', upload.single('file'), function (req, res, next) {
    // console.log(req.file);
    if (!req.file) {
      res.status(500);
      return next(err);
    }
    res.json({ fileUrl: 'http://127.0.0.1:3000/images/' + req.file.filename });
  })

  app.get('/images/:imageId', function (req, res, next) {
    const image = fs.readFileSync('./public/images/image-1579642686850.jpg');// + req.params.beerId);
    res.set('Content-Type', 'image/jpeg');
    res.send(image);
  })

  // app.route('/images/:imageId')
  //   .get(image.getImage)
  //   .post(image.saveImage)
  //   .delete(image.deleteImage);
}
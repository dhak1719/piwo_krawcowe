export default function makeListBeers({ beersDb }) {
    return async function listBeers(beersInfo = {}) {
        const beers = await beersDb.find();
        return beers;
    }
}
"use strict";
import makeListBeers from './listBeers';
import { beersDb } from '../data-access';

const listBeers = makeListBeers({ beersDb });

const beerService = Object.freeze({
    listBeers
});

export default beerService;
export { listBeers };
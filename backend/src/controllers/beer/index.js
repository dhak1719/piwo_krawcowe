'use strict';
import makeGetBeers from './getBeers';
import {
    listBeers
} from '../../services';


const getBeers = makeGetBeers({
    listBeers
});

const beerController = Object.freeze({
    getBeers
});

export default beerController;
export {
    getBeers
};
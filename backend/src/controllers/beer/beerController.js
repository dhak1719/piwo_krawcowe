'use strict';

var mongoose = require('mongoose'),
  Beer = mongoose.model('beer');

var exports = module.exports;

exports.listAllBeer = function (req, res) {
  Beer.find({}, null, {
    sort: {
      date: -1
    }
  }, function (err, beer) {
    if (err)
      res.send(err);
    res.json(beer);
  });
};

exports.readBeer = function (req, res) {
  Beer.findById(req.params.beerId, null, function (err, beer) {
    if (err)
      res.send('Opps' + err)
    res.json(beer)
  });
};

exports.createBeer = function (req, res) {
  setTimeout(function () {
    var newBeer = new Beer(req.body);
    newBeer.save(function (err, beer) {
      if (err)
        res.send(err)
      res.json(beer)
    });
  }, 1000);
};

exports.updateBeer = function (req, res) {
  setTimeout(function () {
    let beerEntity = req.body;
    beerEntity.updatedAt = new Date();

    Beer.findOneAndUpdate({
      _id: req.params.beerId
    }, beerEntity, {
      new: true,
      useFindAndModify: false
    }, function (err, beer) {
      if (err)
        res.send(err)
      res.json(beer)
    });
  }, 1000);
};

exports.deleteBeer = function (req, res) {
  Beer.deleteOne({
    _id: req.params.beerId
  }, function (err, beer) {
    if (err)
      res.send(err)
    res.json({
      message: 'Beer successfully deleted'
    });
  });
};
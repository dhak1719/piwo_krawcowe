'use strict';

export default function makeGetBeers({
    listBeers
}) {
    return async function getBeers(httpRequest) {
        const headers = {
            'Content-Type': 'application/json'
        };
        try {
            const beers = await listBeers();
            // Beer.find({}, null, {
            // sort: {
            // date: -1
            // }
            // })
            return {
                headers,
                statusCode: 200,
                body: beers
            }
        } catch (e) {
            console.log(e);
            return {
                headers,
                statusCode: 400,
                body: {
                    error: e.message
                }
            }
        }
    }
}
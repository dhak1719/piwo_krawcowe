'use strict';

var mongoose = require('mongoose'),
  Temperature = mongoose.model('temperature');

var exports = module.exports;


exports.getTemperatures = function (req, res) {
  var limit = parseInt(req.query.limit) || 50;

  Temperature.find({}, null, {
      sort: {
        date: -1
      },
      limit: limit
    },
    function (error, temperatures) {
      if (error)
        res.send(error);
      res.json(temperatures);
    });
};


exports.saveTemperatures = function (req, res) {
  var newTemperatures = req.body;

  Temperature.insertMany(newTemperatures,
    function (error, temperatures) {
      if (error)
        res.send(error);
      res.sendStatus(200);
    });
};
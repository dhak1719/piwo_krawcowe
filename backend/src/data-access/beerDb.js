export default function makeBeersDb({
    beerModel
}) {
    return Object.freeze({
        find
    })
    async function find() {
        const beers = await beerModel.find({}).exec();
        return beers;
        // beerModel.find({}, null, {
        //     sort: {
        //         date: -1
        //     }
        // }, function (err, beers) {
        //     if (err) {
        //         console.log(err)
        //         return err;
        //     }
        //     return beers;
        // }).;
    }
}
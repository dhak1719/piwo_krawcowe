import makeBeersDb from "./beerDb";
import {
    beerModel
} from '../models';



const beersDb = makeBeersDb({
    beerModel
});

const beerDAO = Object.freeze({
    beersDb
});

export default beerDAO;
export {
    beersDb
};
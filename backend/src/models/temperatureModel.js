'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TemperatureSchema = new Schema({
  date: { type: Date, default: Date.now },
  temperature: Number
});

module.exports = mongoose.model('temperature', TemperatureSchema);
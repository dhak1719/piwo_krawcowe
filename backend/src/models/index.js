import beerModel from './beerModel';

const models = Object.freeze({
    beerModel
});

export default models;
export { beerModel };
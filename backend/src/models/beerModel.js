import mongoose from 'mongoose';

var BeerSchema = new mongoose.Schema({
  title: String,
  name: String,
  author: String,
  description: String,
  photos: [{
    path: String
  }],
  date: {
    type: Date,
    default: Date.now
  },
  comments: [{
    body: String,
    date: Date
  }],
  hidden: Boolean
}, {
  timestamps: true
});

const beer = mongoose.model('beer', BeerSchema);
export default beer;
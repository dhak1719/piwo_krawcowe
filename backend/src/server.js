// 'use strict';

import express from 'express';
// var express = require('express');
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import Logger from './utils/Logger';
// import cors from 'cors';
// Beer = require('./src/models/beerModel');
// Temperature = require('./src/models/temperatureModel'),
// multer = require('multer'),
// beerRoutes = require('./src/routes/beerRoutes'),
// temperatureRoutes = require('./src/routes/temperatureRoutes');
import {
  getBeers
} from './controllers/beer';
import makeCallback from './express-callback'

var app = express();
var port = process.env.port || 3000;
var host = process.env.host || '127.0.0.1';



// mongoose instance connection url connection
mongoose.Promise = global.Promise;
// mongoose.connect('mongodb://localhost/piwokrawcowe', {
mongoose.connect('mongodb://localhost/Tododb', {
  useNewUrlParser: true
});

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// // app.use(cors());
// app.use(function (req, res, next) {
//   res.header('Access-Control-Allow-Origin', '*');
//   res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   if (req.method === 'OPTIONS') {
//     res.sendStatus(200);
//   } else {
//     next();
//   }
// });

// beerRoutes(app);
// temperatureRoutes(app);
// imageRoutes(app, upload);
// notFound(app);

app.route('/beers').get(makeCallback(getBeers));

app.use(function (req, res) {
  res.status(404).send({
    url: req.originalUrl + ' not found'
  })
});

app.listen(port, host);

const logger = new Logger();
logger.logMessage('Server started on: ' + port);